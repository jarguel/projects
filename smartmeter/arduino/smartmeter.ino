#include <avr/sleep.h>
#include <avr/power.h>
#include <MsTimer2.h>
#include <QueueArray.h>
#include <XBee.h>
#include <SoftwareSerial.h>
#include <Time.h>

#define P_DIODE            2
#define P_XBEE_CTS        11
#define P_XBEE_SLEEP_RQ   12
#define P_XBEE_RESET      14
#define P_LED             13

#define PAYLOAD_SIZE      10  //sizeof(payload_t)

#define SLOT_TIME_SEC           60  //Time resolution in seconds
#define NB_TX_TRY               1
#define NB_CONSECUTIVE_FAIL     5

#define SH_SMARTMETER_SAMPLE    0x10
#define SH_TIME_SYNC_REQ        0xA0
#define SH_TIME_SYNC_ACK        0xA1
#define SH_RECEIVER_ACK         0xAA

//#define DEBUG

//---------------------
// Variables
//---------------------
// create the XBee object
XBee xbee = XBee();

//Serial for debug
#ifdef DEBUG
SoftwareSerial debug(7, 8);
#endif

typedef struct {
  uint8_t   msgType;
  uint16_t  msgID;
  uint8_t   nbSlot;
  uint16_t  wattCnt;
  uint32_t  timestamp;
} payload_t;

typedef struct {
  uint8_t   nbSlot;
  uint16_t  wattCnt;
  uint32_t  timestamp;
} queue_member_t;

payload_t payload;
uint8_t   buffer[PAYLOAD_SIZE];

XBeeAddress64       addr64     = XBeeAddress64(0x0013a200, 0x408695CD);
ZBTxRequest         dataTx     = ZBTxRequest(addr64, buffer, sizeof(buffer));
ZBTxStatusResponse  txStatus   = ZBTxStatusResponse();
ZBRxResponse        rx         = ZBRxResponse();
AtCommandRequest    atRequest  = AtCommandRequest();
AtCommandResponse   atResponse = AtCommandResponse();

uint32_t     wattCnt;
uint32_t     wattCntPrevious;
uint16_t     msgID;
bool         readyToSend;
bool         TxSuccess;
bool         TxPending;
uint8_t      TxTry;
uint8_t      nbFail;
uint8_t      check_ok;
QueueArray <queue_member_t> queue;

//---------------------
// Functions
//---------------------
bool readPacket();
int requestTimeSync();

void flashLed(int pin, int times, int wait) {
  for (int i = 0; i < times; i++) {
    digitalWrite(pin, HIGH);
    delay(wait);
    digitalWrite(pin, LOW);
    if (i + 1 < times)
      delay(wait);
    }
}

inline void xbeeReset(void) {
  digitalWrite(P_XBEE_RESET, 0);
  delay(500);
  digitalWrite(P_XBEE_RESET, 1);
}

inline void xbeeWakeUp(void) {
  digitalWrite(P_XBEE_SLEEP_RQ, 0);
}

inline void xbeeWaitForReady(void) {
  while(digitalRead(P_XBEE_CTS));
  //TODO implement timeout
}

inline void xbeeSleep(void) {
  digitalWrite(P_XBEE_SLEEP_RQ, 1);
}

void isrFlash() {
  digitalWrite(P_LED, 1);
  wattCnt++;
  digitalWrite(P_LED, 0);
}

void isrTimer() {
  queue_member_t  elmt;
  uint32_t        tmp;

  #ifdef DEBUG
  debug.println("isrTimer");
  #endif

  //atomic access
  tmp = wattCnt;
  
  //populate queue element
  elmt.timestamp  = now();
  elmt.wattCnt    = (uint16_t)(tmp - wattCntPrevious);
  elmt.nbSlot     = 1;

  //TODO for now queue size is increased when full, need to modify the lib to have fixed queue size
  //if(!queue.isFull())
  queue.push(elmt);

  wattCntPrevious = tmp;
  readyToSend=true;
}


//---------------------
// Arduino setup
//---------------------
void setup() {
  
  //Init counters
  wattCnt=0;
  wattCntPrevious=0;
  msgID=0;
  readyToSend=false;
  TxSuccess=false;
  TxPending=false;
  TxTry=0;
  
  memset(&payload, 0, sizeof(payload));
  
  #ifdef DEBUG
  debug.begin(9600);
  debug.println("\n\n**SmartMeter start**");
  #endif

  //Configure pins
  pinMode(P_LED           , OUTPUT);
  pinMode(P_DIODE         , INPUT);
  pinMode(P_XBEE_SLEEP_RQ , OUTPUT);
  pinMode(P_XBEE_CTS      , INPUT);
  pinMode(P_XBEE_RESET    , OUTPUT);
 
  digitalWrite(P_LED, 1);

  //Configure isr for LED pin
  attachInterrupt(0, isrFlash, RISING);

  //WakeUp
  xbeeWakeUp();
  xbeeReset();
  xbeeWaitForReady();
  
  //Power down unused modules
  power_adc_disable();
  power_spi_disable();
  power_timer1_disable();
  power_twi_disable();
  
  //Configure IDLE mode, not deeper mode because need to have timer0 and timer2 running
  set_sleep_mode(SLEEP_MODE_IDLE);
  
  //Configure timer isr for sending counter to xbee
  MsTimer2::set(299410, isrTimer);  //5min period
  MsTimer2::start();

  // Startup delay to wait for XBee radio to initialize.
  delay(10000);
  
  //Configure xbee serial 
  Serial.begin(9600);
  xbee.setSerial(Serial);
 
  while(requestTimeSync() != 0) {
    delay(5000);
    #ifdef DEBUG
    debug.println("Time sync failed !");
    #endif
  }
  xbeeSleep();
  power_usart0_disable();
}


//---------------------
// Arduino loop
//---------------------
void loop() {

  queue_member_t elmt;
  uint32_t       time;

  if(readyToSend==true) {
    power_usart0_enable();
    xbeeWakeUp();

    // Will try to send NB_TX_TRY times max, if still failing will try again after the next Timer interrupt
    TxTry=NB_TX_TRY;
    while((TxTry>0) && ( !queue.isEmpty() || TxPending==true)) {
      TxTry--;
      while(readPacket());  //read until timeout occurs
      //Pop from queue is no data pending
      if(TxPending==false) {
        elmt = queue.pop();

        payload.msgType   = SH_SMARTMETER_SAMPLE;
        payload.msgID     = msgID;
        payload.timestamp = elmt.timestamp;
        payload.nbSlot    = elmt.nbSlot;
        payload.wattCnt   = elmt.wattCnt;
        memcpy(&buffer, &payload, sizeof(buffer));
      }

      //Send message
      #ifdef DEBUG
      debug.print("Send message... ");
      #endif
      TxSuccess=false;
      TxPending=true;
      xbeeWaitForReady();
      #ifdef DEBUG
      for (int i = 0; i < sizeof(buffer); i++) {
         debug.print(buffer[i], HEX);
         debug.print(" ");
      }
      #endif
      xbee.send(dataTx);

      //Check status response
      if (xbee.readPacket(2000)) {
        if (xbee.getResponse().isAvailable()) {
          if (xbee.getResponse().getApiId() == ZB_TX_STATUS_RESPONSE) {
            xbee.getResponse().getZBTxStatusResponse(txStatus);
            if (txStatus.getDeliveryStatus() == SUCCESS)
              TxSuccess=true;
          } 
        }
        #ifdef DEBUG 
        else if (xbee.getResponse().isError()) {
          debug.print("XBee error. error code is");
          debug.println(xbee.getResponse().getErrorCode(), DEC);
        }
        #endif
      }

      //Check ACK from receiver
      check_ok = -1;
      if(TxSuccess==true) {
        #ifdef DEBUG
        debug.print("check ack... ");
        #endif
        if (xbee.readPacket(5000)) {
          if (xbee.getResponse().isAvailable()) {
            if (xbee.getResponse().getApiId() == ZB_RX_RESPONSE) {
              #ifdef DEBUG
              for (int i = 0; i < xbee.getResponse().getFrameDataLength(); i++) {
                debug.print(xbee.getResponse().getFrameData()[i], HEX);
                debug.print(" ");
              }
              #endif
              xbee.getResponse().getZBRxResponse(rx);
              if(rx.getData()[0]==SH_RECEIVER_ACK) {
                //Check it contents the msgID we sent
                check_ok  = 0;
                check_ok += rx.getData()[1] != (msgID & 0xff);
                check_ok += rx.getData()[2] != ((msgID>>8) & 0xff);
                //get remote time
                time  = (uint32_t)(rx.getData()[3]);
                time += (uint32_t)(rx.getData()[4])<<8;
                time += (uint32_t)(rx.getData()[5])<<16;
                time += (uint32_t)(rx.getData()[6])<<24;
                #ifdef DEBUG
                debug.print("Received time is ");
                debug.println(time, DEC); 
                #endif
                setTime(time);
              }
            }
          }
          #ifdef DEBUG 
          else if (xbee.getResponse().isError()) {
            debug.print("XBee error. error code is");
            debug.println(xbee.getResponse().getErrorCode(), DEC);
          }
          #endif
        }
      }

      if(check_ok==0) {
        #ifdef DEBUG
        debug.println(" success!");
        #endif
        TxPending=false;
        TxTry=NB_TX_TRY;
        msgID++;
      } else {
        #ifdef DEBUG
        debug.println(" failed!");
        #endif
        if(TxTry!=0)
          delay(5000); //wait before trying again
        else
          nbFail++;
      }
    }
    //Reset xbee if too many fails
    if(nbFail>=NB_CONSECUTIVE_FAIL) {
      #ifdef DEBUG
      debug.println("Reset xbee...");
      #endif
      xbeeReset();
      xbeeWaitForReady();
      nbFail=0;
    }
    readyToSend=false;

    //Check if received any extra data
    //Commented for now since we don't expect data
    //while(readPacket());  //read until timeout occurs

    xbeeSleep();
    power_usart0_disable();
  } 
  //CPU enter sleep now
  sleep_enable();
  sleep_mode();

  //Waking-up... disable sleep
  sleep_disable();
}


bool readPacket() {
  
  bool ret;

  ret=xbee.readPacket(1000);

  if (xbee.getResponse().isAvailable()) {
    #ifdef DEBUG
    debug.print("API=");
    debug.print(xbee.getResponse().getApiId(), HEX);
    debug.print(" : ");
    #endif
    if (xbee.getResponse().getApiId() == ZB_RX_RESPONSE) {
      xbee.getResponse().getZBRxResponse(rx);
      #ifdef DEBUG
      for (int i = 0; i < xbee.getResponse().getFrameDataLength(); i++) {
        debug.print(xbee.getResponse().getFrameData()[i], HEX);
        debug.print(" ");
      }
      #endif
    }
    #ifdef DEBUG
    debug.println("");
    #endif
  }
  #ifdef DEBUG 
  else if (xbee.getResponse().isError()) {
    debug.print("XBee error. error code is");
    debug.println(xbee.getResponse().getErrorCode(), DEC);
  }
  #endif

  return ret;
}


int requestTimeSync() {

  uint32_t    time;
  uint8_t     buf[1];
  ZBTxRequest timeTx = ZBTxRequest(addr64, buf, sizeof(buf));

  while(readPacket());  //read until timeout occurs

  buf[0] = SH_TIME_SYNC_REQ;
  xbee.send(timeTx);

  //Check status response
  if (xbee.readPacket(1000)) {
    if (xbee.getResponse().isAvailable()) {
      if (xbee.getResponse().getApiId() == ZB_TX_STATUS_RESPONSE) {
        xbee.getResponse().getZBTxStatusResponse(txStatus);
        if (txStatus.getDeliveryStatus() == SUCCESS)
          TxSuccess=true;
      } 
    }
    else if (xbee.getResponse().isError()) {
      #ifdef DEBUG 
      debug.print("XBee error. error code is");
      debug.println(xbee.getResponse().getErrorCode(), DEC);
      #endif
      return -1;
    }
  }

  if (xbee.readPacket(1000)) {
    if (xbee.getResponse().isAvailable()) {
      if (xbee.getResponse().getApiId() == ZB_RX_RESPONSE) {
        #ifdef DEBUG
        for (int i = 0; i < xbee.getResponse().getFrameDataLength(); i++) {
          debug.print(xbee.getResponse().getFrameData()[i], HEX);
          debug.print(" ");
        }
        #endif
        xbee.getResponse().getZBRxResponse(rx);
        if(rx.getData()[0]==SH_TIME_SYNC_ACK) {
          //get remote time
          time  = (uint32_t)(rx.getData()[1]);
          time += (uint32_t)(rx.getData()[2])<<8;
          time += (uint32_t)(rx.getData()[3])<<16;
          time += (uint32_t)(rx.getData()[4])<<24;
          #ifdef DEBUG
          debug.print("Received time is ");
          debug.println(time, DEC); 
          #endif
          setTime(time);
          return 0;
        }
      }
    } 
    #ifdef DEBUG
    else if (xbee.getResponse().isError()) {
      debug.print("XBee error. error code is");
      debug.println(xbee.getResponse().getErrorCode(), DEC);
    }
    #endif
  }

  return -1;
}
