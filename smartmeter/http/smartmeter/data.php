<?php
date_default_timezone_set('Europe/Paris');

// get the parameters
$callback = $_GET['callback'];
if (!preg_match('/^[a-zA-Z0-9_]+$/', $callback)) {
  die('Invalid callback name');
}

$last = $_GET['last'];
if($last && !preg_match('/^[0-1]+$/', $last)) {
  die("Invalid last parameter: $last");
}

$start = $_GET['start'];
if ($start && !preg_match('/^[0-9]+$/', $start)) {
  die("Invalid start parameter: $start");
}

$end = $_GET['end'];
if ($end && !preg_match('/^[0-9]+$/', $end)) {
  die("Invalid end parameter: $end");
}
if (!$end) $end = mktime() * 1000;

// connect to MySQL
require_once($_SERVER['DOCUMENT_ROOT'].'/db_conf.php');
$con = mysql_connect($db_host,$db_user,$db_password);
if (!$con) {
    die('Could not connect: ' . mysql_error());
}
mysql_select_db("smarthouse", $con);


if ($last) {
  $table = 'smartmeter';
  $sql = "
    select 
    watt_cnt * 12 as watt_cnt
    from $table 
    order by date_received desc
    limit 1
  ";
  
  $result = mysql_query($sql) or die(mysql_error());
  
  $rows = array();
  while ($row = mysql_fetch_assoc($result)) {
    extract($row);
    $rows[] = "[$watt_cnt]";
  }
} else {
  // set some utility variables
  $range = $end - $start;
  $startTime = date('Y-m-d H:i:s', $start / 1000); // JS to MySQL
  $endTime = date('Y-m-d H:i:s', $end / 1000);
  
  // find the right table
  // two days range loads minute data
  //if ($range < 2 * 24 * 3600 * 1000) {
  //  $table = 'stockquotes';
  //// one month range loads hourly data
  //} elseif ($range < 31 * 24 * 3600 * 1000) {
  //  $table = 'stockquotes_hour';
  //// one year range loads daily data
  //} elseif ($range < 12 * 31 * 24 * 3600 * 1000) {
  //  $table = 'stockquotes_day';
  //// greater range loads monthly data
  //} else {
  //  $table = 'stockquotes_month';
  //} 
  $table = 'smartmeter';
  
  $sql = "
    select 
    unix_timestamp(date_received) * 1000 as date_received,
    watt_cnt * 12 as watt_cnt
    from $table 
    where date_received between '$startTime' and '$endTime'
    order by date_received
    limit 5000
  ";
  
  $result = mysql_query($sql) or die(mysql_error());
  
  $rows = array();
  while ($row = mysql_fetch_assoc($result)) {
    extract($row);
    $rows[] = "[$date_received,$watt_cnt]";
  }
}

// print it
header('Content-Type: text/javascript');

echo "/* start = $start, end = $end, last=$last, \$_GET['start'] = $_GET[start], \$_GET['end'] = $_GET[end], startTime = $startTime, endTime = $endTime */";
echo $callback ."([\n" . join(",\n", $rows) ."\n]);";

mysql_close($con);
?>

