<!DOCTYPE HTML>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <title>SmartHouse</title>
      <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
 <!--     <script type="text/javascript" src="../js/highstock.js">        </script> -->
 <!--     <script type="text/javascript" src="../js/highcharts-more.js">  </script> -->
 <!--     <script type="text/javascript" src="../js/exporting.js">        </script> -->
      <script src="http://code.highcharts.com/stock/highstock.js">          </script>
      <script src="http://code.highcharts.com/highcharts-more.js">          </script>
      <script src="http://code.highcharts.com/stock/modules/exporting.js">  </script>
      <script type="text/javascript" src="../js/themes/grid.js"></script>
      <script type="text/javascript">

      Highcharts.setOptions({
         global: {
              useUTC: false
                 }
      });

      $.getJSON('data.php?callback=?', function(data) {
         $('#container1').highcharts('StockChart', {
             chart : {
                renderTo : 'container1',
                type: 'spline',
                zoomType: 'x'
             },

             navigator : {
                adaptToUpdatedData: false,
                series : {
                   data : data
                }
             },

             scrollbar : {
               liveRedraw : false
             },

             title: {
                text: 'Power Consumption'
             },

             rangeSelector : {
                buttons: [{
                   type: 'hour',
                   count: 1,
                   text: '1h'
                }, {
                   type: 'day',
                   count: 1,
                   text: '1d'
                },{
                   type: 'week',
                   count: 1,
                   text: '1w'
                },{
                   type: 'month',
                   count: 1,
                   text: '1m'
                }, {
                   type: 'year',
                   count: 1,
                   text: '1y'
                }, {
                   type: 'all',
                   text: 'All'
                }],
                inputEnabled: true, // it supports only days
                selected : 2 // week
             },

             xAxis : {
                events : {
                   afterSetExtremes : afterSetExtremes
                },
                minRange: 3600 * 1000, // one hour
                title: {
                  text: 'Local Time'
                }
             },

             yAxis : {
               min : 0,
               title: {
                text: 'Power (W)'
               }
             },

             series : [{
                name : 'Power',
                data : data,
                dataGrouping: {
                   enabled: false
                }
             }]
          });
      });

      /*
        * GAUGE
      */
      $.getJSON('data.php?last=1&callback=?', function(data) {
         $('#container2').highcharts({
            chart: {
                renderTo : 'container2',
                type: 'gauge',
                plotBackgroundColor: null,
                plotBackgroundImage: null,
                plotBorderWidth: 0,
                plotShadow: false
            },
            
            title: {
                text: 'Live Power'
            },

            subtitle: {
              text: 'Last 5 minutes average'
            },

            exporting: {
              enabled: false
            },

            pane: {
                startAngle: -150,
                endAngle: 150,
                background: [{
                    backgroundColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                        stops: [
                            [0, '#FFF'],
                            [1, '#333']
                        ]
                    },
                    borderWidth: 0,
                    outerRadius: '109%'
                }, {
                    backgroundColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                        stops: [
                            [0, '#333'],
                            [1, '#FFF']
                        ]
                    },
                    borderWidth: 1,
                    outerRadius: '107%'
                }, {
                    // default background
                }, {
                    backgroundColor: '#DDD',
                    borderWidth: 0,
                    outerRadius: '105%',
                    innerRadius: '103%'
                }]
            },
               
            // the value axis
            yAxis: {
                min: 0,
                max: 8000,
                
                minorTickInterval: 'auto',
                minorTickWidth: 1,
                minorTickLength: 10,
                minorTickPosition: 'inside',
                minorTickColor: '#666',
      
                tickPixelInterval: 30,
                tickWidth: 2,
                tickPosition: 'inside',
                tickLength: 10,
                tickColor: '#666',
                labels: {
                    step: 2,
                    rotation: 'auto'
                },
                title: {
                    text: 'Watt'
                },
                plotBands: [{
                    from: 0,
                    to: 1500,
                    color: '#55BF3B' // green
                }, {
                  from: 1500,
                    to: 4500,
                    color: '#DDDF0D' // yellow
                }, {
                    from: 4500,
                    to: 7000,
                    color: '#ED7F10' // orange
                }, {
                    from: 7000,
                    to: 8000,
                    color: '#DF5353' // red
                }]        
            },
      
            series: [{
                name: 'Power',
                data: data[0],
                tooltip: {
                    valueSuffix: ' W'
                }
            }]
         });
      });

      function afterSetExtremes(e) {
         var url,
            currentExtremes = this.getExtremes(),
            range = e.max - e.min;
         var chart = $('#container1').highcharts();
           
         chart.showLoading('Loading data from server...');
         $.getJSON('data.php?start='+ Math.round(e.min) +
                  '&end='+ Math.round(e.max) +'&callback=?', function(data) {
                       
            chart.series[0].setData(data);
            chart.hideLoading();
         });
      }


      </script>
   </head>
   <body>
      <div id="container1" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
      <div id="container2" style="max-width: 400px; height: 300px; margin-top: 10px"></div>
   </body>
</html>


