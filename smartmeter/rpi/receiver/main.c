#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <time.h>

#include <mysql/mysql.h>
#include <xbee.h>

#define XBEE_DEVICE_ADDR_HI     0x0013A200
#define XBEE_DEVICE_ADDR_LO     0x408695D1

#define SH_SMARTMETER_SAMPLE    0x10
#define SH_TIME_SYNC_REQ        0xA0
#define SH_TIME_SYNC_ACK        0xA1
#define SH_RECEIVER_ACK         0xAA

#define MAX_FAILS               3

typedef struct {
  uint8_t   id;
  uint16_t  msgID;
  uint8_t   nbSlot;
  uint16_t  wattCnt;
  uint32_t  timestamp;
} sm_sample_t;

int nb_fails=0;
uint16_t last_msgID=-1;

int iodb_insert(sm_sample_t *payload) {
  MYSQL *dbcon;
  MYSQL_RES *res;
  MYSQL_ROW row;
  char *server = "localhost";
  char *user = "jeremie";
  char *password = "smartmeter";
  char *database = "smarthouse";
  char query[256];



  /* Connect to database */
  dbcon = mysql_init(NULL);
  if (!mysql_real_connect(dbcon, server, user, password, database, 0, NULL, 0)) {
    fprintf(stderr, "%s\n", mysql_error(dbcon));
    return -1;
  }

  snprintf(query, sizeof(query), "INSERT INTO `smarthouse`.`smartmeter` \
        (`msg_id`, `nb_slot`, `watt_cnt`, `date_sample`) VALUES ('%u', '%u', '%u', FROM_UNIXTIME(%u));",\
        payload->msgID, payload->nbSlot, payload->wattCnt, payload->timestamp);

  /* send SQL query */
  if (mysql_query(dbcon, query)) {
    fprintf(stderr, "%s\n", mysql_error(dbcon));
    return -1;
  }

  /* Close connection */
  mysql_close(dbcon);

  return 0;
}
  

void rxCB(struct xbee *xbee, struct xbee_con *xcon, struct xbee_pkt **pkt, void **data) {
  sm_sample_t *payload;
  char        buffer[8];
  xbee_err    ret;
  char        retVal;
  int         i;
  time_t      t_now, t_received, t_sample; 
  char        buf[80];
  struct tm   *tm_received;
  xbee_log(xbee, 100, "Entering callback");

  if ((*pkt)->dataLen > 0) {

    switch((*pkt)->data[0]) {

      case SH_TIME_SYNC_REQ:
        //get current time
        time(&t_now);
        t_now+=2; //to compensate time sync delay (empiric)
        //create message
        buffer[0] = SH_TIME_SYNC_ACK;
        for(i=0; i<4; i++)
          buffer[i+1] = (t_now>>(i*8)) & 0xff;
        //send message
        xbee_log(xbee, 100, "Send time sync message %d", t_now);
        if ((ret = xbee_connTx(xcon, &retVal, buffer, 5)) != XBEE_ENONE) {
          xbee_log(xbee, -1, "xbee_connTx() returned: %d (%s)", ret, xbee_errorToStr(ret));
          xbee_log(xbee, -1, "xbee_connTx() retVal: %x\n", retVal);
          nb_fails++;
        }
        break;

      case SH_SMARTMETER_SAMPLE:
        if((payload = malloc(sizeof(sm_sample_t)))==NULL) {
          xbee_log(xbee, -1, "payload malloc returned NULL\n");
          return;
        }

        time(&t_received);
        tm_received = localtime(&t_received);

        payload->id         = (*pkt)->data[0];
        payload->msgID      = ((*pkt)->data[1]) | (((*pkt)->data[2])<<8);
        payload->nbSlot     = ((*pkt)->data[3]);
        payload->wattCnt    = ((*pkt)->data[4]) | (((*pkt)->data[5])<< 8);
        payload->timestamp  = ((*pkt)->data[6]) | (((*pkt)->data[7])<< 8) | (((*pkt)->data[8])<<16) | (((*pkt)->data[9])<<24);

        printf("#%02d:%02d:%02d ; ", tm_received->tm_hour, tm_received->tm_min, tm_received->tm_sec);
        printf("%d ; "    , payload->id);
        printf("0x%x ; "  , payload->msgID);
        printf("%d ; "    , payload->nbSlot);
        printf("%d ; "    , payload->wattCnt);
        t_sample = payload->timestamp;
        strftime(buf, sizeof(buf), "%H:%M:%S", localtime(&t_sample));
        printf("%s\n"     , buf);
        
        //Insert into database if not already done
        if(payload->msgID != last_msgID) {
          if(iodb_insert(payload)) {
            xbee_log(xbee, -1, "failed to insert into database\n");
            free(payload);
            return;
          }
          last_msgID = payload->msgID;
        }

        time(&t_now); //get time
        t_now+=2;     //to compensate time sync delay (empiric)
        //Send acknowledge
        buffer[0] = SH_RECEIVER_ACK;
        //acknowledge contains the msgID of the message to be acked
        buffer[1] =  payload->msgID     & 0xff;
        buffer[2] = (payload->msgID>>8) & 0xff;
        //..and the current time for resync
        for(i=0; i<4; i++)
          buffer[i+3] = (t_now>>(i*8)) & 0xff;

        xbee_log(xbee, 100, "Send ack");
        if ((ret = xbee_connTx(xcon, &retVal, buffer, 7)) != XBEE_ENONE) {
          xbee_log(xbee, -1, "xbee_connTx() returned: %d (%s)", ret, xbee_errorToStr(ret));
          xbee_log(xbee, -1, "xbee_connTx() retVal: %x\n", retVal);
          nb_fails++;
        }
        free(payload);
        break;
    }
  }
  xbee_log(xbee, 100, "Exiting callback");
}


int main(void) {
  
  struct xbee *xbee;
  struct xbee_con *xcon;
  struct xbee_conAddress address;
  xbee_err ret;
  int i;
  
  nb_fails=0;
  setvbuf(stdout, NULL, _IOLBF, 128);

  /* Connect to xbee */
  if ((ret = xbee_setup(&xbee, "xbee2", "/dev/ttyUSB0", 115200)) != XBEE_ENONE) {
    printf("ret: %d (%s)\n", ret, xbee_errorToStr(ret));
    return ret;
  }

  //if (xbee_logLevelSet(xbee, 101) != XBEE_ENONE) return;

  memset(&address, 0, sizeof(address));
  address.addr64_enabled = 1;
  for(i=0; i<4; i++) address.addr64[i  ] = (XBEE_DEVICE_ADDR_HI>>(8*(3-i))) & 0xff;
  for(i=0; i<4; i++) address.addr64[i+4] = (XBEE_DEVICE_ADDR_LO>>(8*(3-i))) & 0xff;
  
  //create connection
  if ((ret = xbee_conNew(xbee, &xcon, "Data", &address)) != XBEE_ENONE) {
    xbee_log(xbee, -1, "xbee_conNew() returned: %d (%s)", ret, xbee_errorToStr(ret));
    return ret;
  }
  
  //install callback
  if ((ret = xbee_conCallbackSet(xcon, rxCB, NULL)) != XBEE_ENONE) {
    xbee_log(xbee, -1, "xbee_conCallbackSet() returned: %d", ret);
    return ret;
  }

  printf("Date ; id ; msgID ; nbSlot ; wattCnt ; timestamp\n");

  while(1) {
    sleep(1);

    //check successive fails
    if(nb_fails>=MAX_FAILS) {
      
      xbee_log(xbee, -1, "Maximum succesive fails reached. Close connection and try again.");
      
      //end connection
      if ((ret = xbee_conCallbackSet(xcon, NULL, NULL)) != XBEE_ENONE) {
        xbee_log(xbee, -1, "xbee_conCallbackSet() returned: %d", ret);
        return ret;
      }
		  if ((ret = xbee_conEnd(xcon)) != XBEE_ENONE) {
		    xbee_log(xbee, -1, "xbee_conEnd() returned: %d", ret);
		    return ret;
	    }
      xbee_shutdown(xbee);

      //init again
      if ((ret = xbee_setup(&xbee, "xbee2", "/dev/ttyUSB0", 115200)) != XBEE_ENONE) {
        printf("ret: %d (%s)\n", ret, xbee_errorToStr(ret));
        return ret;
      }
      if ((ret = xbee_conNew(xbee, &xcon, "Data", &address)) != XBEE_ENONE) {
        xbee_log(xbee, -1, "xbee_conNew() returned: %d (%s)", ret, xbee_errorToStr(ret));
        return ret;
      }
      if ((ret = xbee_conCallbackSet(xcon, rxCB, NULL)) != XBEE_ENONE) {
        xbee_log(xbee, -1, "xbee_conCallbackSet() returned: %d", ret);
        return ret;
      }
    nb_fails=0;
    }
  }
  return 0;
}
